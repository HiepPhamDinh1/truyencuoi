package com.nac.lesson11k7laughstory.entity;

public class Topic {
    private final String idName;
    private final String title;

    public Topic(String idName, String title) {
        this.idName = idName;
        this.title = title;
    }

    public String getIdName() {
        return idName;
    }

    public String getTitle() {
        return title;
    }
}
