package com.nac.lesson11k7laughstory;

import com.nac.lesson11k7laughstory.entity.Story;

import java.util.List;

public class Storage {

    private List<Story> m002ListStory;
    private Story m002Story;

    public void setM002ListStory(List<Story> listStory) {
        m002ListStory = listStory;
    }
    public void setM002Story(Story story) {
        m002Story = story;
    }

    public List<Story> getM002ListStory() {
        return m002ListStory;
    }

    public Story getM002Story() {
        return m002Story;
    }
}
