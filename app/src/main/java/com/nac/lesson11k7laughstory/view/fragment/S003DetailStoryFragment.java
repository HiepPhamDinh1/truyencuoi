package com.nac.lesson11k7laughstory.view.fragment;

import android.widget.TextView;

import androidx.viewpager.widget.ViewPager;

import com.nac.lesson11k7laughstory.App;
import com.nac.lesson11k7laughstory.R;
import com.nac.lesson11k7laughstory.entity.Story;
import com.nac.lesson11k7laughstory.view.adapter.DetailStoryAdapter;
import com.nac.lesson11k7laughstory.viewmodel.DetailViewModel;

import java.util.List;

public class S003DetailStoryFragment extends BaseFragment<DetailViewModel> {
    private ViewPager vpStory;
    private List<Story> listStory;
    private Story story;
    private TextView tvIndex;

    @Override
    protected void initViews() {
        tvIndex = rootView.findViewById(R.id.tv_index);
        vpStory = rootView.findViewById(R.id.vp_story);
        initData();
    }

    @Override
    protected Class<DetailViewModel> getClassViewModel() {
        return DetailViewModel.class;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.m003_detail_story;
    }

    public List<Story> getListStory() {
        return listStory;
    }

    public void setListStory(List<Story> listStory) {
        this.listStory = listStory;
    }

    private void initData() {
        DetailStoryAdapter adapter
                = new DetailStoryAdapter(listStory, getContext());

        vpStory.setAdapter(adapter);

        int pos = listStory.indexOf(story);
        vpStory.setCurrentItem(pos, true);
        tvIndex.setText(String.format("%s/%s", (pos + 1), listStory.size()));

        vpStory.addOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int pos) {
                Story story = listStory.get(pos);
                App.getInstance().getStorage().setM002Story(story);

                tvIndex.setText(String.format("%s/%s", (pos + 1), listStory.size()));
            }
        });
    }

    public void setStory(Story story) {
        this.story = story;
    }
}
