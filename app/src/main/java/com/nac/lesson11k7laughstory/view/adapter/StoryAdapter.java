 package com.nac.lesson11k7laughstory.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.nac.lesson11k7laughstory.R;
import com.nac.lesson11k7laughstory.entity.Story;

import java.util.List;

public class StoryAdapter extends RecyclerView.Adapter<StoryAdapter.StoryHolder> {
    private final List<Story> listStory;
    private final Context mContext;
    private OnItemClick callBack;
    private Story selectedStory;

    public StoryAdapter(List<Story> listStory, Context context) {
        this.listStory = listStory;
        this.mContext = context;
    }

    @NonNull
    @Override
    public StoryHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(mContext)
                .inflate(R.layout.item_story, parent, false);

        return new StoryHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull StoryHolder holder, int position) {
        Story data = listStory.get(position);
        holder.tvName.setText(data.getName());
        holder.lnStory.setBackgroundResource(data.isSelected()
                ? R.color.colorGreenLight : R.color.white);
        holder.story = data;
    }

    @Override
    public int getItemCount() {
        return listStory.size();
    }

    public void delStory(Story story) {
        listStory.remove(story);
        notifyDataSetChanged();
    }

    public void setOnItemClick(OnItemClick event) {
        callBack = event;
    }

    public void setSelectedStory(Story story) {
        if(selectedStory!=null){
            selectedStory.setSelected(false);
        }

        story.setSelected(true);
        selectedStory = story;
        notifyDataSetChanged();
    }

    public interface OnItemClick {
        void onItemClick(Story story);
    }

    public class StoryHolder extends RecyclerView.ViewHolder {
        TextView tvName;
        LinearLayout lnStory;
        Story story;

        public StoryHolder(@NonNull View itemView) {
            super(itemView);
            itemView.setOnClickListener(v -> {
                v.startAnimation(AnimationUtils
                        .loadAnimation(mContext, R.anim.anim_click));

                if(selectedStory!=null){
                    selectedStory.setSelected(false);
                }
                story.setSelected(true);
                notifyDataSetChanged();
                selectedStory = story;

                if (callBack != null) {
                    callBack.onItemClick(story);
                }
            });
            tvName = itemView.findViewById(R.id.tv_name);
            lnStory = itemView.findViewById(R.id.ln_story);
        }
    }
}
