package com.nac.lesson11k7laughstory.view.fragment;

import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.nac.lesson11k7laughstory.App;
import com.nac.lesson11k7laughstory.OnActionCallBack;
import com.nac.lesson11k7laughstory.R;
import com.nac.lesson11k7laughstory.entity.Story;
import com.nac.lesson11k7laughstory.entity.Topic;
import com.nac.lesson11k7laughstory.view.adapter.StoryAdapter;
import com.nac.lesson11k7laughstory.viewmodel.MainViewModel;

public class S002MainFragment extends BaseFragment<MainViewModel> implements StoryAdapter.OnItemClick {
    public static final String KEY_SHOW_DETAIL_STORY = "KEY_SHOW_DETAIL_STORY";
    private DrawerLayout drawer;
    private RecyclerView rvStory;

    private OnActionCallBack callBack;

    @Override
    protected void initViews() {
        findViewById(R.id.iv_menu, this);
        rvStory = findViewById(R.id.rv_story);
        rvStory.setLayoutManager(new LinearLayoutManager(getContext()));

        drawer = findViewById(R.id.drawer);
        LinearLayout lnTopic = findViewById(R.id.ln_topic);
        lnTopic.removeAllViews();

        mModel.initData();
        for (Topic item : mModel.getListTopic()) {
            View v = initTopicView(item);
            v.setOnClickListener(v1 -> clickItem(item));
            lnTopic.addView(v);
        }
        initAdapter();
    }

    public void setCallBack(OnActionCallBack callBack) {
        this.callBack = callBack;
    }


    @Override
    protected Class<MainViewModel> getClassViewModel() {
        return MainViewModel.class;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.m002_act_main;
    }

    private void initAdapter() {
        if (mModel.getListStory() == null) return;

        StoryAdapter storyAdapter = new StoryAdapter(mModel.getListStory(), getContext());
        storyAdapter.setOnItemClick(this);
        rvStory.setAdapter(storyAdapter);
    }

    private void clickItem(Topic item) {
        Toast.makeText(getContext(), "Topic: " + item.getTitle(), Toast.LENGTH_SHORT).show();
        //Get list story
        mModel.initListStory(item.getTitle());

        initAdapter();

        drawer.closeDrawers();
    }

    private View initTopicView(Topic item) {
        View view = LayoutInflater.from(getContext())
                .inflate(R.layout.item_topic, null, false);
        ImageView ivIcon = view.findViewById(R.id.iv_icon);
        TextView tvName = view.findViewById(R.id.tv_name);

        try {
            Glide.with(this).load(BitmapFactory.decodeStream(App.getInstance().getAssets().open(item.getIdName()))).into(ivIcon);
            tvName.setText(item.getTitle());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return view;
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.iv_menu) {
            drawer.openDrawer(GravityCompat.START);
        }
    }

    @Override
    public void onItemClick(Story story) {
        callBack.onCallBack(KEY_SHOW_DETAIL_STORY, story, mModel.getListStory());
    }
}
