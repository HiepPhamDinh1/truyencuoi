package com.nac.lesson11k7laughstory;

public interface OnActionCallBack {
    void onCallBack (String key, Object... listObj);
}
